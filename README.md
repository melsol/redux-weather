# Redux Weather Forecast App

This is a demo site to build a weather forecasting app using React and Redux

## Publically hosted copy
Open your browser at http://sleepy-cove-61179.herokuapp.com/

## Running locally
TODO: test this on a proper linux/Mac machine

1. `git clone git@bitbucket.org:melsol/redux-weather.git`
1. `cd redux-weather`
1. `npm install`
1. `npm start` This will run the site at http://localhost:3000
1. `npm run build` will rebuild the app after making changes

## Running via vagrant
This is useful for running an Ubuntu virtual machine on a Windows machine

1. install Virtualbox https://www.virtualbox.org/
1. Install vagrant https://www.vagrantup.com/
1. `git clone git@bitbucket.org:melsol/redux-weather.git`
1. `cd redux-weather`
1. `vagrant up` this may take some time the first time it's run
1. `vagrant ssh` this logs in to the ubuntu virtual machine
1. `cd /vagrant`
1. `npm start` This will run the site at http://localhost:3000
1. `npm run build` will rebuild the app after making changes
1. You can open the site via a browser on your host (windows) machine at http://weather.manteka.org:3000

## Testing
* To run tests `npm test`
* To run coverage report `npm run coverage`

## TODO
* Handle API error response
* Add more forecast details
* Use charting library to graph temperature changes etc.
* Implement tests (possibly using jest). Need to learn more about React and Redux testing first
* Implement Jenkins job or other CI to run tests and make built branch, instead of manually building to `dist` folder
* Use icon font for weather symbols: https://erikflowers.github.io/weather-icons/
* Replace Bootstrap with lightweight custom CSS
* Use PostCSS + CSSNext
* gulp tasks to concat and minify static assets
* gulp tasks for linting js and css
* gulp task for auto-prefixer
* gulp task to watch files and rebuild after changes when developing
* Add custom domain to Heroku app, and (maybe) put a cdn (e.g. Cloudfront) in front of static assets

## Notes
* Using git bash on windows instead of normal cmd terminal
* Run git bash as administrator (right-click >> Run as Adminstrator). This is needed for the node_modules symlink
* Public site will fail over https due to insecure content. API only seems to work via http
