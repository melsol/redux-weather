
const config = {
    API_KEY: process.env.API_KEY || 'b69a1586a28ebef83a00e7bd857eae60',
    BASE_API_URL: process.env.BASE_API_URL || 'http://api.openweathermap.org/data/2.5/forecast?id=',
    CITYID: process.env.CITYID || '2648579' // Glasgow
};

export default config
