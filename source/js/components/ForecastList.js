
import React, { PropTypes, Component } from 'react';
import Forecast from './Forecast';

const ForecastList = ({forecasts}) => {
    return (<ul className="forecast-list">
    {
        forecasts.map((fc, i) =>
        <Forecast key={i} forecast={fc} />
    )}
    </ul>);
};

ForecastList.propTypes = {
    forecasts: PropTypes.array.isRequired
};

export default ForecastList
