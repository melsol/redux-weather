
import React, { PropTypes, Component } from 'react';

const Summary = ({city, lastUpdated}) => {
    let h1 = city ? `5-day weather forecast for ${city.name}` : `Loading...`;
    let updated = lastUpdated ? `Last updated at ${new Date(lastUpdated).toLocaleTimeString()}.` : '';
    return (
        <div className="panel panel-default">
            <div className="panel-body">
                <h1>{h1}</h1>
                <p>{updated}</p>
            </div>
        </div>
    );
};

Summary.propTypes = {
    city: PropTypes.object,
    lastUpdated: PropTypes.number
};

export default Summary
