
import React, { PropTypes, Component } from 'react';

const Forecast = ({forecast}) => {
    let forecastDate = new Date(forecast.dt * 1000);
    let title = `${forecastDate.toLocaleDateString()} ${forecastDate.toLocaleTimeString()}`;
    return (
    <li className="forecast panel panel-info">
        <h2 className="panel-heading">{title}</h2>
        <div className="panel-body">{forecast.weather[0].main}</div>
    </li>);
};

Forecast.propTypes = {
    forecast: PropTypes.object.isRequired
};

export default Forecast
