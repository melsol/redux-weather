
import {
    REQUEST_FORECASTS, RECEIVE_FORECASTS
} from './actions';

function rootReducer(state = {
    isFetching: false,
    city: null,
    forecasts: null,
    lastUpdated: null
}, action) {
    switch (action.type) {
        case REQUEST_FORECASTS:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_FORECASTS:
            return Object.assign({}, state, {
                isFetching: false,
                city: action.city,
                forecasts: action.forecasts,
                lastUpdated: action.receivedAt
            });
        default:
        return state;
    }
}

export default rootReducer
