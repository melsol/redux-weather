
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchForecastsIfNeeded } from '../actions';
import ForecastList from '../components/ForecastList';
import Summary from '../components/Summary';

class AsyncApp extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.log('componentDidMount!!');
        const { dispatch } = this.props;
        dispatch(fetchForecastsIfNeeded());
    }

    render() {
        const { city, forecasts, isFetching, lastUpdated } = this.props;
        return (
            <div>
            <Summary city={city} isFetching={isFetching} lastUpdated={lastUpdated} />
            {isFetching && (!forecasts || forecasts.length === 0) &&
                <h2>Loading...</h2>
            }
            {!isFetching && (!forecasts || forecasts.length === 0) &&
                <h2>Empty.</h2>
            }
            {forecasts && forecasts.length > 0 &&
                <div style={{ opacity: isFetching ? 0.5 : 1 }}>
                <ForecastList forecasts={forecasts} />
                </div>
            }
            </div>
        );
    }
}

AsyncApp.propTypes = {
    forecasts: PropTypes.array,
    isFetching: PropTypes.bool.isRequired,
    lastUpdated: PropTypes.number,
    city: PropTypes.object,
    dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    const {
        forecasts,
        city,
        isFetching,
        lastUpdated
    } = state;

    return {
        forecasts,
        city,
        isFetching,
        lastUpdated
    }
}

export default connect(mapStateToProps)(AsyncApp)
