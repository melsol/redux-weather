
import fetch from 'isomorphic-fetch';
import config from './config';

export const REQUEST_FORECASTS = 'REQUEST_FORECASTS'
export const RECEIVE_FORECASTS = 'RECEIVE_FORECASTS'

function requestForecasts() {
    return {
        type: REQUEST_FORECASTS
    };
}

function receiveForecasts(json) {
    return {
        type: RECEIVE_FORECASTS,
        city: json.city,
        forecasts: json.list,
        receivedAt: Date.now()
    };
}

 // TODO: refactor url building, so that it's easier to use a different service
 // TODO handle error recieving json
function fetchForecasts() {
    return dispatch => {
        dispatch(requestForecasts());
        return fetch(config.BASE_API_URL + config.CITYID + '&appid=' + config.API_KEY)
        .then(response => response.json())
        .then(json => dispatch(receiveForecasts(json)));
    };
}

function shouldFetchForecasts(state) {
    console.log('shouldFetchForecasts!!', state);
    const forecasts = state.forecasts;
    if (!forecasts) {
        return true;
    } else if (state.isFetching) {
        return false;
    } else {
        return state.didInvalidate;
    }
}

export function fetchForecastsIfNeeded() {
    console.log('fetchForecastsIfNeeded!!');
    return (dispatch, getState) => {
        if (shouldFetchForecasts(getState())) {
            return dispatch(fetchForecasts());
        }
    };
}
