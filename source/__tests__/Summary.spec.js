
jest
    .dontMock('../js/components/Summary')
    .dontMock('./Wrap');

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

const Summary = require('../js/components/Summary').default;
const Wrap = require('./Wrap').default;

describe('Render a Summary component', () => {
    it('renders the city name and update time', () => {
        const city = {name: 'Glasgow'};
        const lastUpdated = new Date();
        const summary = TestUtils.renderIntoDocument(
            <Wrap><Summary lastUpdated={lastUpdated.getTime()} city={city} /></Wrap>
        );
        const h1 = TestUtils.findRenderedDOMComponentWithTag(summary, 'h1');
        expect(h1.innerHTML).toContain('Glasgow');
        const updatePara = TestUtils.findRenderedDOMComponentWithTag(summary, 'p');
        expect(updatePara.innerHTML).toContain(lastUpdated.toLocaleTimeString());
    });
});
